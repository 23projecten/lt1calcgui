package multiformat;

/**
 * Represents a numeric base of 8
 * 
 * @author David Sherman
 * @author Stephan Roffel
 * @version 1.0
 */
public class OctalBase extends Base {
	
	/**
	 * Creates an OctalBase object
	 */
	public OctalBase() {
		super("octal", 8, "01234567");
	}

}
