/*
 * (C) Copyright 2005 Davide Brugali, Marco Torchiano
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307  USA
 */
package multiformat;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * The multiformat calculator
 */
public class Calculator {
  private Stack<Rational> operands = new Stack<>();
  
  private int ammount = 0;
  private List<ActionListener> actionListeners = new ArrayList<>();
  
  // The current format of the calculator
  private Format format = new FixedPointFormat();
  private Format oldformat; // previous format value
  // The current numberbase of the calculator
  private Base base = new DecimalBase();
  private Base oldbase; // previews base format
  
  public void addOperand(String newOperand) throws FormatException, NumberBaseException {
      Rational r = format.parse(newOperand, base);
      operands.push(r);
      
      triggerEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
  }

  public void add(){
	if(operands.isEmpty()) return;  
	  
	Rational r = operands.pop();
    while(!operands.isEmpty()) {
    	r = r.plus(operands.pop());
    }
    operands.push(r);
    
    ammount++;
    triggerEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
  }
  public void subtract() {
	  if(operands.isEmpty()) return;
	  
	  Rational r = operands.pop();
	    while(!operands.isEmpty()) {
	    	r = r.minus(operands.pop());
	    }
	    operands.push(r);
    
    ammount++;
    triggerEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
  }
  public void multiply() {
	  if(operands.isEmpty()) return;
	  
	  Rational r = operands.pop();
	    while(!operands.isEmpty()) {
	    	r = r.mul(operands.pop());
	    }
	    operands.push(r);
    
    ammount++;
    triggerEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
  }
  public void divide() throws FormatException {
	if(operands.isEmpty()) return;
    
	Rational r = operands.pop();
    while(!operands.isEmpty()) {
    	r = r.div(operands.pop());
    }
    operands.push(r);
	
	ammount++;
	triggerEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
  }
  
  public void delete() {
	if(operands.isEmpty()) return;
    operands.pop();
    
    triggerEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
  }

  public String lastOperand() {
    return operands.isEmpty() ? "0.0" : format.toString(operands.peek(),base);
  }
  
  public void addActionListener(ActionListener listener) {
	  actionListeners.add(listener);
  }
  
  public void removeActionListener(ActionListener listener) {
	  actionListeners.remove(listener);
  }
  
  public void triggerEvent(ActionEvent e) {
	  for(ActionListener listner : actionListeners) {
		  listner.actionPerformed(e);
	  }
  }
  
  public String convert(String value) throws NumberBaseException, FormatException {
	  if(oldbase == null) oldbase = base;
	  if(oldformat == null) oldformat = format;
	  
	  Rational r = oldformat.parse(value, oldbase);
	  return format.toString(r, base);
  }
  
  public void reset() {
	  operands.clear();
	  
	  triggerEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
  }
  
  public int getAmmount() {
	  return this.ammount;
  }
  
  public Base getBase(){
    return base;
  }
  
  public void setBase(Base newBase){
	oldbase = base;
    base = newBase;
    
    triggerEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
  }
  
  public Format getFormat(){
    return format;
  }
  
  public void setFormat(Format newFormat){
	oldformat = format;
    format = newFormat;
    
    triggerEvent(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, null));
  }
}