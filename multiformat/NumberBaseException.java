package multiformat;

/**
 * Exception thrown if the the entered value doensn't match the base value
 * @author David Sherman
 * @version 1.0
 *
 */
public class NumberBaseException extends Exception {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Creates a NumberBaseException 
	 * @param message
	 */
	public NumberBaseException(String message) {
		super(message);
	}
}
