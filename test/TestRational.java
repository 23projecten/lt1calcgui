package test;

import static org.junit.Assert.*;
import multiformat.FormatException;
import multiformat.Rational;

import org.junit.Before;
import org.junit.Test;

/**
 * JUnit Testcase to test Rational. 
 * Note that this class uses 'annotations' (the @...). This is a Java 1.5 feature. 
 * @author gebruiker
 *
 */
public class TestRational {
	Rational r;
	Rational r2;
	
	@Before
	public void setUp(){
		r = new Rational();
		r2 = new Rational();
	}
	
	@Test
	public void testSimplify() {
		r.setNumerator(25);
		r.setDenominator(5);
		r.simplify();
		
		assertTrue(r.getNumerator() == 5.0);
		assertTrue(r.getDenominator() == 1.0);
		
		r.setNumerator(10);
		r.setDenominator(0.5);
		r.simplify();
		
		assertTrue(r.getNumerator() == 10.0);
		assertTrue(r.getDenominator() == 0.5);		
	}
	
	@Test
	public void testDevide() {
		Rational ra = new Rational(0.0, 5.0);
		Rational ra2 = new Rational(0.0, 8.0);
		
		assertTrue(testDevide(ra, ra2, false));
		
		ra2.setNumerator(5.0);
		
		assertTrue(testDevide(ra, ra2, true));
		
		ra.setNumerator(10.0);
		
		assertTrue(testDevide(ra, ra2, true));
		
		ra2.setDenominator(0.0);
		
		assertTrue(testDevide(ra, ra2, false));
	}
	
	public boolean testDevide(Rational r1, Rational r2, boolean expectedResult) {
		try {
			r1.div(r2);
			return expectedResult ? true : false;
		} catch(FormatException e) {
			return !expectedResult ? true : false;
		}
	}
	
	@Test
	public void testCanonical() {
		r.setNumerator(12.5);
		r.setDenominator(1.0);
		r.canonical();
		
		assertTrue(r.getNumerator() == 125.0);
		assertTrue(r.getDenominator() == 10.0);

		r.setNumerator(12.5);
		r.setDenominator(0.01);
		r.canonical();
		
		assertTrue(r.getNumerator() == 125.0);
		assertTrue(r.getDenominator() == 0.1);	
	}
	
	@Test
	public void testCanonicalAndSimplify() {
		r.setNumerator(12.5);
		r.setDenominator(1.0);
		r.canonical();
		r.simplify();
		
		assertTrue(r.getNumerator() == 25.0);
		assertTrue(r.getDenominator() == 2.0);		
	}



}
