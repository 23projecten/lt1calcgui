package test;

import static org.junit.Assert.*;

import java.awt.SecondaryLoop;

import multiformat.Calculator;
import multiformat.DecimalBase;
import multiformat.FormatException;
import multiformat.NumberBaseException;
import multiformat.OctalBase;
import multiformat.Rational;

import org.junit.Before;
import org.junit.Test;

public class TestBase {
	
	Rational r1;
	Rational r2;
	
	Calculator c;
	private boolean testCatch;
	
	@Before
	public void setUp() {
		r1 = new Rational();
		c = new Calculator();
	}

	@Test
	public void testOctal() {
		c.setBase(new OctalBase());
		
		try {
			c.addOperand("7");
			c.addOperand("3");
			c.add();
			
			String str = c.lastOperand();
			assertTrue(str.equals("12.0"));
			
			c.addOperand("1");
			c.add();
			str = c.lastOperand();
			assertTrue(str.equals("13.0"));
			
			c.setBase(new DecimalBase());
			str = c.lastOperand();
			assertTrue(str.equals("11.0"));
			
			testCatch = true;
			
			c.setBase(new OctalBase());
			c.addOperand("9");
			fail("should not reach this point");
		} catch (FormatException e) {
			fail("Unexpected FormateException");
		} catch (NumberBaseException e) {
			if (!testCatch) fail("should not enter catch");
		}
	}

}
