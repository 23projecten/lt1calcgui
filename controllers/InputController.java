package controllers;

import java.awt.GridLayout;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;

import ui.DisplayView;

/**
 * Group of buttons which controls the input given to the Calculator
 * @author David Sherman
 * @version 1.0
 *
 */
public class InputController extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private DisplayView display;
	private List<String> characters;
	private JPanel mainContainer;
	private JPanel keyContainer;
	
	/**
	 * Creates an InputController object
	 * @param characters Characters which can be entered
	 * @param display Display view
	 */
	public InputController(String characters, DisplayView display) {
		this.characters = Arrays.asList(characters.split(""));
		this.display = display;
		
		this.mainContainer = new JPanel(new GridLayout(0, 1));
		this.add(mainContainer);
		
		this.keyContainer = new JPanel(new GridLayout(0, 3));
		mainContainer.add(keyContainer);
		
		for(String s : this.characters) {
			final JButton btn = new JButton(s);
			btn.addActionListener(e -> { display.append(btn.getText()); });
			
			this.keyContainer.add(btn);
		}
	}
}
