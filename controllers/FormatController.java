package controllers;

import java.awt.GridLayout;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;









import ui.GUI;
import multiformat.*;

/**
 * Group of buttons which controls the format used by the Calculator
 * @author David Sherman
 * @version 1.0
 *
 */
public class FormatController extends JPanel {
	
	private static final long serialVersionUID = 3560126273190583556L;
	
	private Calculator calc;
	private GUI gui;
	
	private JButton fixedButton = new JButton("fixed");
	private JButton floatButton = new JButton("float");
	private JButton ratButton = new JButton("rat");
	
	/**
	 * Creates a FormatController object
	 * @param gui GUI container object
	 */
	public FormatController(GUI gui) {
		this.calc = gui.getCalculator();
		this.gui = gui;
		
		this.setLayout(new GridLayout(0,1));
		
		addButtons();
		addListeners();	
	}
	
	/**
	 * Adds buttons to JPanel
	 */
	private void addButtons() {
		this.add(fixedButton);
		this.add(floatButton);
		this.add(ratButton);
	}

	/**
	 * Adds action listeners to all the buttons
	 */
	private void addListeners() {
		fixedButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				calc.setFormat(new FixedPointFormat());
			}
		});
		
		floatButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				calc.setFormat(new FloatingPointFormat());
			}
		});
		
		ratButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				calc.setFormat(new RationalFormat());
			}
		});	
	}
}
