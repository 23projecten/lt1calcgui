package controllers;

import java.awt.GridLayout;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;









import ui.GUI;
import multiformat.*;

/**
 * Group of buttons which controls the base used by the Calculator
 * @author David Sherman
 * @version 1.0
 *
 */
public class BaseController extends JPanel {
	
	private static final long serialVersionUID = 3560126273190583556L;
	
	private Calculator calc;
	private GUI gui;
	
	private JButton decButton = new JButton("dec");
	private JButton binButton = new JButton("bin");
	private JButton hexButton = new JButton("hex");
	private JButton octButton = new JButton("octa");
	
	/**
	 * Creates a BaseController object
	 * @param gui GUI container object
	 */
	public BaseController(GUI gui) {
		this.calc = gui.getCalculator();
		this.gui = gui;
		
		this.setLayout(new GridLayout(0,1));
		
		addButtons();
		addListeners();	
	}
	
	/**
	 * Adds buttons to JPanel
	 */
	private void addButtons() {
		this.add(decButton);
		this.add(binButton);
		this.add(hexButton);
		this.add(octButton);
	}

	/**
	 * Adds action listeners to all the buttons
	 */
	private void addListeners() {
		decButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				calc.setBase(new DecimalBase());
				gui.setInput(new InputController("123456789.0", gui.getDisplay()));
			}
		});
		
		binButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				calc.setBase(new BinaryBase());
				gui.setInput(new InputController("01.", gui.getDisplay()));
			}
		});
		
		hexButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				calc.setBase(new HexBase());
				gui.setInput(new InputController("0123456789ABCDEF.", gui.getDisplay()));
			}
		});
		
		octButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				calc.setBase(new OctalBase());
				gui.setInput(new InputController("1234567.0", gui.getDisplay()));
			}
		});		
	}
}
