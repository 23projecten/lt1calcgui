package controllers;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import ui.GUI;
import multiformat.Calculator;
import multiformat.FormatException;
import multiformat.NumberBaseException;

/**
 * 
 * @author School
 *
 */
public class OperatorController extends JPanel {
	private static final long serialVersionUID = 1L;

	private GUI gui;
	private Calculator calc;
	
	private JPanel gridContainer;
	private JButton clrButton = new JButton("C");
	private JButton ceButton = new JButton("CE");
	private JButton backButton = new JButton("<-");
	private JButton addButton = new JButton("+");
	private JButton minButton = new JButton("-");
	private JButton mulButton = new JButton("x");
	private JButton divButton = new JButton("/");
	private JButton setButton = new JButton("Set");
	
	/**
	 * Controls the different operators used by calculator
	 * @param gui GUI container class
	 */
	public OperatorController(GUI gui) {
		this.calc = gui.getCalculator();
		this.gui = gui;
		
		gridContainer = new JPanel(new GridLayout(0,2));
		this.add(gridContainer);
		
		addButtons();
		addListeners();
	}
	
	/**
	 * Sets the ActionListeners for the buttons
	 */
	private void addListeners() {
		clrButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				gui.getDisplay().clear();
			}
		});
		
		ceButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				calc.reset(); 
				gui.getDisplay().clear();
			}
		});
		
		backButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				gui.getDisplay().back();
			}
		});
		
		addButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				calc.add();
			}
		});
		
		minButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				calc.subtract();
			}
		});
		
		mulButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				calc.multiply();
			}
		});
		
		divButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					calc.divide();
				} catch(FormatException exc) {
					gui.getDisplay().setMessage(exc.getMessage());
				}
			}
		});
		
		setButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(gui.getDisplay().getText().length() <= 0) return;
				
				try {
					calc.addOperand(gui.getDisplay().getText());
				} catch (FormatException | NumberBaseException e) {
					gui.getDisplay().setMessage(e.getMessage());
				}
				gui.getDisplay().clear();
			}
		});
	}

	/**
	 * Adds the buttons to the container
	 */
	private void addButtons() {
		gridContainer.add(addButton);
		gridContainer.add(ceButton);
		gridContainer.add(minButton);
		gridContainer.add(clrButton);
		gridContainer.add(mulButton);
		gridContainer.add(backButton);
		gridContainer.add(divButton);
		gridContainer.add(setButton);
	}
}
