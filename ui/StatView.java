package ui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

import multiformat.Calculator;

/**
 * View that provides basic statistics about the calculators use
 * @author David Sherman
 * @version 1.0
 *
 */
public class StatView extends JPanel implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	
	private JTextField ammountField;

	public StatView() {
		this.setLayout(new GridLayout(0,1));
		
		this.ammountField = new JTextField();
		this.ammountField.setEditable(false);
		
		this.add(ammountField);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Calculator c = (Calculator)e.getSource();
		int ammount = c.getAmmount();
		ammountField.setText("Ammount of operations: " + ammount);
	}

}
