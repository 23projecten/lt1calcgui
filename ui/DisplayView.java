package ui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

import multiformat.Calculator;
import multiformat.FormatException;
import multiformat.NumberBaseException;

/**
 * Main view used by the program, displays entered numbers and the current operands
 * @author David Sherman
 * @version 1.0
 *
 */
public class DisplayView extends JPanel implements ActionListener {
	 
	private static final long serialVersionUID = 1L;
	
	private JTextField lastOperandArea = new JTextField();
	private JTextField messageArea = new JTextField();
	
	private JTextField inputTextArea;
	
	/**
	 * Creates a DisplayView object
	 */
	public DisplayView() {
		this.setLayout(new GridLayout(0,1));
		
		this.add(lastOperandArea);
		lastOperandArea.setEditable(false);
		this.add(messageArea);
		messageArea.setEditable(false);
		
		this.inputTextArea = new JTextField();
		this.add(inputTextArea);
		inputTextArea.setEditable(false);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Calculator c = (Calculator)e.getSource();
		setMessage("");
		
		setLastOperandText(c.lastOperand());
		
		if(getText().length() <= 0) return;
		
		try {
			setText(c.convert(getText()));
		} catch (FormatException | NumberBaseException exc) {
			setMessage(exc.getMessage());
		}
	}
	
	/**
	 * Appends given string to end of input area. Ignores a second period.
	 * @param str String to append
	 */
	public void append(String str) {
		if(str.equals(".") && hasPeriod()) return;
		this.inputTextArea.setText(inputTextArea.getText()+str);
	}
	
	/**
	 * Checks of input area contains a period
	 * @return True if it has perios, else false;
	 */
	private boolean hasPeriod() {
		return hasText() ? inputTextArea.getText().indexOf(".") >= 0 : false;
	}
	
	/**
	 * Checks whether input area has text
	 * @return True if has text, else false
	 */
	private boolean hasText() {
		return inputTextArea.getText().length() > 0;
	}
	
	/**
	 * Removes last character from input area
	 */
	public void back() {
		if(!hasText()) return;
		inputTextArea.setText(inputTextArea.getText().substring(0, inputTextArea.getText().length()-1));
	}
	
	/**
	 * Clears text from input area and message area
	 */
	public void clear() {
		inputTextArea.setText("");
		messageArea.setText("");
	}
	
	public void setLastOperandText(String text) {
		lastOperandArea.setText("Last Operand: " + text);
	}
	
	public String getText() {
		return inputTextArea.getText();
	}
	
	public void setText(String text) {
		inputTextArea.setText(text);
	}
	
	public String getMessage() {
		return messageArea.getText();
	}
	
	public void setMessage(String msg) {
		messageArea.setText(msg);
	}
}
