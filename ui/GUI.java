package ui;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import multiformat.Calculator;
import controllers.BaseController;
import controllers.FormatController;
import controllers.InputController;
import controllers.OperatorController;

/**
 * Container class for all the different GUI elements
 * @author David Sherman
 * @version 1.0
 *
 */
public class GUI extends JFrame {

	private static final long serialVersionUID = -3404752537656379374L;
	
	private JPanel container;
	
	private InputController number;
	private DisplayView display;
	private StatView stats;
	private OperatorController operators;
	private JPanel westContainer;
	private BaseController base;
	private FormatController format;
	
	private Calculator calc;
	
	/**
	 * Creates an instance of the GUI class
	 */
	public GUI(Calculator calc) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
			/* Default look and feel */
		}
		
		this.container = (JPanel)this.getContentPane();
		this.container.setLayout(new BorderLayout());
		
		this.calc = calc;
		this.display = new DisplayView();
		this.stats = new StatView();
		this.calc.addActionListener(display);
		this.calc.addActionListener(stats);
		this.number = new InputController("123456789.0", display);
		this.operators = new OperatorController(this);
		
		this.westContainer = new JPanel();
		this.base = new BaseController(this);
		this.westContainer.add(this.base);
		this.format = new FormatController(this);
		this.westContainer.add(this.format);
		
		this.container.add(this.display, BorderLayout.NORTH);
		this.container.add(this.number, BorderLayout.CENTER);
		this.container.add(this.operators, BorderLayout.EAST);
		this.container.add(this.westContainer, BorderLayout.WEST);
		this.container.add(this.stats, BorderLayout.SOUTH);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.pack();
		this.repaint();
		this.setVisible(true);
	}
	
	/**
	 * Returns the calculator object 
	 * @return
	 */
	public Calculator getCalculator() {
		return this.calc;
	}
	
	/**
	 * Returns the main display element
	 * @return
	 */
	public DisplayView getDisplay() {
		return this.display;
	}
	
	/**
	 * Sets the input controller
	 * @param number
	 */
	public void setInput(InputController number) {
		this.remove(this.number);
		this.number = number;
		
		this.container.add(this.number, BorderLayout.CENTER);
		
		this.pack();
		this.repaint();
	}
}
